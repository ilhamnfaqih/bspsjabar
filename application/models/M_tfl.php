<?php 

class M_tfl extends CI_Model{	
	function data(){		
		$token = $this->session->userdata('token_user');
		return $query = $this->db->query("SELECT *,tb_tfl.nama_lengkap AS nama_tfl FROM tb_tfl 
											LEFT JOIN tb_korfas ON
											tb_korfas.NIK = tb_tfl.NIK_korfas
											WHERE tb_korfas.token_user = '$token'
		ORDER BY tgl_spk_kontrak DESC ")->result();
	}

	function show_drive($NIK_tfl){
		return $query = $this->db->query("SELECT * FROM drive_tfl
		WHERE NIK_tfl = '$NIK_tfl'")->result();
	}

	function data_drive_single(){		
		$token = $this->session->userdata('token_user');
		return $query = $this->db->query("SELECT * FROM tb_tfl
											WHERE tb_tfl.token_user = '$token'
		")->row();
	}

	function data_drive(){		
		$token = $this->session->userdata('token_user');
		return $query = $this->db->query("SELECT * FROM drive_tfl 
											INNER JOIN tb_tfl ON
											tb_tfl.NIK_tfl = drive_tfl.NIK_tfl
											WHERE tb_tfl.token_user = '$token'
		ORDER BY tgl_spk_kontrak DESC ")->result();
	}

	function edit($NIK_tfl){
		return $query = $this->db->query("SELECT * FROM tb_tfl 
		WHERE NIK_tfl ='$NIK_tfl'
		")->row();
	}

	function update_data($where,$data,$table){		
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function input_drive($data,$table){
		$this->db->insert($table,$data);
	}
}