<?php 

class M_pb extends CI_Model{	
	function data($kota){		
		return $query = $this->db->query("SELECT * FROM penerima_bantuan INNER JOIN
		tb_tfl ON penerima_bantuan.NIK_tfl = tb_tfl.NIK_tfl
		WHERE kotkab = '$kota'
										ORDER BY penerima_bantuan.NIK_tfl DESC 
										")->result();
	}

	function update_data_foto($where,$data,$table){		
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function data_on_tfl($kota){		
		$token_tfl = $this->session->userdata('token_user');

		return $query = $this->db->query("SELECT * FROM penerima_bantuan INNER JOIN
		tb_tfl ON penerima_bantuan.NIK_tfl = tb_tfl.NIK_tfl INNER JOIN
		user ON tb_tfl.token_user = user.token_user
		WHERE kotkab = '$kota' AND tb_tfl.token_user = '$token_tfl'
										ORDER BY penerima_bantuan.NIK_tfl DESC 
										")->result();
	}

	function by_tfl($NIK_tfl){		
		return $query = $this->db->query("SELECT * FROM penerima_bantuan INNER JOIN
		tb_tfl ON penerima_bantuan.NIK_tfl = tb_tfl.NIK_tfl
										WHERE penerima_bantuan.NIK_tfl = '$NIK_tfl'
										ORDER BY penerima_bantuan.NIK_tfl DESC 
										")->result();
	}

	function option_tfl($kota){
		$token_user = $this->session->userdata('token_user');
		$getnik = $this->db->query("SELECT NIK FROM tb_korfas 
		WHERE token_user = '$token_user' ")->row()->NIK;

		
		return $query = $this->db->query("SELECT * FROM tb_tfl 
		WHERE NIK_korfas = '$getnik' AND kotkab = '$kota'
		ORDER BY tb_tfl.nama_lengkap ASC ")->result();
	}

	function foto($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp
										ORDER BY nama_foto DESC 
										")->result();
	}

	function foto_pers($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%foto_perspektif.jpg%' ")->result();
	}

	function foto_depan($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%foto_tampak_depan.jpg%' ")->result();
	}

	function foto_kanan($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%foto_tampak_samping_kanan.jpg%' ")->result();
	}

	function foto_kiri($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%foto_tampak_samping_kiri.jpg%' ")->result();
	}

	function foto_belakang($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%foto_tampak_belakang.jpg%' ")->result();
	}
	
	function fondasi($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%fondasi.jpg%' ")->result();
	}

	function sambungan($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%sambungan.jpg%' ")->result();
	}

	function sloof($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%sloof.jpg%' ")->result();
	}

	function overstek($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%overstek.jpg%' ")->result();
	}

	function balok($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%balok.jpg%' ")->result();
	}

	function ikatanbalokdenganrangka($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%ikatan_balok_dengan_rangka.jpg%' ")->result();
	}

	function ikatanangin($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%ikatan_angin.jpg%' ")->result();
	}

	function rangkaatap($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%rangka_atap.jpg%' ")->result();
	}

	function lantai($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%lantai.jpg%' ")->result();
	}

	function dinding($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%dinding.jpg%' ")->result();
	}

	function pintu($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%pintu.jpg%' ")->result();
	}

	function jendela($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%jendela.jpg%' ")->result();
	}

	function sofisofi($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%sofi_sofi.jpg%' ")->result();
	}

	function genteng($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%genteng.jpg%' ")->result();
	}

	function kamartidur($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%kamar_tidur.jpg%' ")->result();
	}

	function kamarmandi($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%kamar_mandi.jpg%' ")->result();
	}

	function dapur($ktp){
		return $query = $this->db->query("SELECT * FROM foto_pb 
		WHERE no_KTP_pb = $ktp AND nama_foto LIKE '%dapur.jpg%' ")->result();
	}

	function export_page($ktp){
		return $query = $this->db->query("SELECT * FROM penerima_bantuan INNER JOIN 
		tb_tfl ON penerima_bantuan.NIK_tfl = tb_tfl.NIK_tfl INNER JOIN
		foto_pb ON penerima_bantuan.no_KTP_pb = foto_pb.no_KTP_pb
										WHERE penerima_bantuan.no_KTP_pb = $ktp
										GROUP BY penerima_bantuan.no_KTP_pb 
										ORDER BY nama_foto DESC 
										")->result();
	}
}