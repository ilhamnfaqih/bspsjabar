<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Monitoring BSPS</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url(); ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
  <link href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url(); ?>assets/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
<?php 
  if (($this->session->userdata('role') == 'admin' )){
?>
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div>
          <i><img class="img-profile rounded-circle" src="<?php echo base_url(); ?>assets/images/logo_pupr.jpeg" width="20px" alt="AVATAR"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Monitoring BSPS</div>
        
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>Admin">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard Progress</span></a>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link " href="<?php echo base_url(); ?>Data_korfas">
          <i class="fas fa-fw fa-database"></i>
          <span>Data Korfas</span>
        </a>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link " href="<?php echo base_url(); ?>Toko_bangunan">
          <i class="fas fa-fw fa-store"></i>
          <span>Toko Bangunan</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link " href="<?php echo base_url(); ?>Laporan">
          <i class="fas fa-fw fa-book"></i>
          <span>Laporan Pengendalian</span>
        </a>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-home"></i>
          <span>Kabupaten / Kota</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
          <?php 
            foreach($rows as $row){
          ?>
            <a class="collapse-item" href="<?php echo base_url(); ?>Admin/pb/<?php echo $row->kotkab; ?>"><?php echo $row->kotkab; ?></a>
          <?php
            }
          ?>
          </div>
        </div>
      </li>

       <!-- Divider -->
       <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link " href="<?php echo base_url(); ?>ChangePassword">
          <i class="fas fa-fw fa-book"></i>
          <span>Edit Kata Sandi </span>
        </a>
      </li>
      
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->
<?php 
  } else if(($this->session->userdata('role') == 'korfas' )){
?>
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div>
          <i><img class="img-profile rounded-circle" src="<?php echo base_url(); ?>assets/images/logo_pupr.jpeg" width="20px" alt="AVATAR"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Korfas Panel</div>
        
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>Korfas">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard Progress</span></a>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link " href="<?php echo base_url(); ?>Data_tfl">
          <i class="fas fa-fw fa-database"></i>
          <span>Data TFL</span>
        </a>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link " href="<?php echo base_url(); ?>Korfas/drive_korfas">
          <i class="fas fa-fw fa-database"></i>
          <span>Data Drive TFL</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link " href="<?php echo base_url(); ?>Laporan">
          <i class="fas fa-fw fa-book"></i>
          <span>Laporan Pengendalian</span>
        </a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">
      <li class="nav-item">
          <small style="margin-left:10px; color:#fff;"> Penerima Bantuan</small>
      </li>
      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-home"></i>
          <span>Kabupaten / Kota</span>
        </a>
        
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
          <?php 
            foreach($rows as $row){
          ?>
            <a class="collapse-item" href="<?php echo base_url(); ?>Admin/pb/<?php echo $row->kotkab; ?>"><?php echo $row->kotkab; ?></a>
          <?php
            }
          ?>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link " href="<?php echo base_url(); ?>ChangePassword">
          <i class="fas fa-fw fa-book"></i>
          <span>Edit Kata Sandi </span>
        </a>
      </li>

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

      </ul>
      <!-- End of Sidebar -->
<?php
  } else{
?>
  <!-- Sidebar -->
  <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
  <div>
    <i><img class="img-profile rounded-circle" src="<?php echo base_url(); ?>assets/images/logo_pupr.jpeg" width="20px" alt="AVATAR"></i>
  </div>
  <div class="sidebar-brand-text mx-3">TFL Panel</div>
  
</a>
<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item">
  <a class="nav-link" href="<?php echo base_url(); ?>Tfl">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Dashboard Progress</span></a>
</li>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
  <a class="nav-link " href="<?php echo base_url(); ?>Data_drive_tfl">
    <i class="fas fa-fw fa-database"></i>
    <span>My TFL Drive</span>
  </a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Divider -->
<hr class="sidebar-divider">
<li class="nav-item">
    <small style="margin-left:10px; color:#fff;"> Penerima Bantuan</small>
</li>

<!-- Nav Item - Utilities Collapse Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
    <i class="fas fa-fw fa-home"></i>
    <span>Kabupaten / Kota</span>
  </a>
  
  <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
    <?php 
      foreach($rows as $row){
    ?>
      <a class="collapse-item" href="<?php echo base_url(); ?>Admin/pb_tfl/<?php echo $row->kotkab; ?>"><?php echo $row->kotkab; ?></a>
    <?php
      }
    ?>
    </div>
  </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider">
<li class="nav-item">
  <a class="nav-link " href="<?php echo base_url(); ?>ChangePassword">
    <i class="fas fa-fw fa-book"></i>
    <span>Edit Kata Sandi </span>
  </a>
</li>

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
<!-- End of Sidebar -->
<?php
  }
?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				        <span class="mr-2 d-none d-lg-inline text-gray-600 small">Hi, <?php echo $this->session->userdata("nama"); ?></span>
                <img class="img-profile rounded-circle" src="<?php echo base_url(); ?>assets/images/logo_pupr.jpeg">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <!--a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a-->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?php echo base_url('welcome/logout'); ?>" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->