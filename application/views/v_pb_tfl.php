<?php
  $this->load->view('head');
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dropzone.css">
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Penerima Bantuan</h1>
          </div>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid">
      <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  
                  <br>
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kecamatan</th>
                      <th>Desa/Kelurahan</th>
                      <th>Nama</th>
                      <th>No. KTP PB</th>
                      <th>Foto Sebelumnya</th>
                      <th>Foto Terbaru</th>
                      <th>Update Foto</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Kecamatan</th>
                      <th>Desa/Kelurahan</th>
                      <th>Nama</th>
                      <th>No. KTP PB</th>
                      <th>Foto Sebelumnya</th>
                      <th>Foto Terbaru</th>
                      <th>Update Foto</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                        $no = 1;
                        foreach($rows2 as $row){
                      ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $row->kecamatan1; ?></td>
                      <td><?php echo $row->deskel1; ?></td>
                      <td><?php echo $row->nama_pb; ?></td>
                      <td><?php echo $row->no_KTP_pb; ?></td>
                      <td>
                          <img src="<?php echo base_url();?>uploads/<?php echo $row->foto_lama; ?>" width="100px" alt=" Tidak ada Foto">
                          <br>
                          <small><b> Tanggal Upload : <?php echo $row->tgl_lama; ?></b></small>
                      </td>
                      <td>
                          <img src="<?php echo base_url();?>uploads/<?php echo $row->foto_baru; ?>" width="100px" alt=" Tidak ada Foto">
                          <br>
                          <small><b> Tanggal Upload : <?php echo $row->tgl_baru; ?></b></small>
                      </td>
                      <td><button class="d-none d-sm-inline-block btn btn-sm btn-warning openModal" data-id="<?php echo $row->no_KTP_pb; ?>"  id="openModal" data-toggle="modal" data-target="#myModal"><i class="fa fa-upload"></i> Upload</button></td>
                    </tr>
                    <?php
                      $no++;
                        }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->

      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Foto</h4>
              <button type="button" class="close" id="closeModal" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <p>Tarik dan Simpan Foto untuk menambahkan. <br></p>
              <div class="image_upload_div">
                  <div class="dropzone">
                  <input type="hidden" name="ktp" id="ktp" value="<?php echo $row->no_KTP_pb; ?>">
                  </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
          </div>

        </div>
      </div>

      <!--- End of Modal --->

<?php
  $this->load->view('foot');
?>
<script src="<?php echo base_url() ?>assets/js/dropzone.js"></script>
<script type="text/javascript">

$('#myModal').on('hidden.bs.modal', function () {
  location.reload();
})

$(document).ready(function() {

  $(".openModal").click(function() {
    //remove previous value
    $("#ktp").val("");

    //this'll set id to your modal input
    $("#ktp").val($(this).attr("data-id"));
  });
  
});

  Dropzone.autoDiscover = false;

  var foto_upload= new Dropzone(".dropzone",{
    url: "<?php echo base_url('Tfl/proses_upload/') ?>",
    maxFilesize: 5,
    resizeWidth: 100,
    quality: 0.6,
    method:"post",
    acceptedFiles:"image/*",
    paramName:"userfile",
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
  });


  //Event ketika Memulai mengupload
  foto_upload.on("sending",function(a,b,c){
    a.token=Math.random();
    c.append("token_foto",a.token); //Menmpersiapkan token untuk masing masing foto
    c.ktp = $('#ktp').val();
    c.append("ktp",c.ktp); 
  });

  
</script>
