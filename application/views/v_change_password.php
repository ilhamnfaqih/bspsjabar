<?php
  $this->load->view('head');
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Ubah Kata Sandi / Password</h1></div>
            <p><a class="d-none d-sm-inline-block btn btn-sm btn-primary" href="<?php echo base_url();?>Data_tfl"><i class="fas fa-arrow-left text-white-50"></i> Kembali</a></p>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid col-md-6">
      <div class="card shadow mb-4">
      
            <div class="card-body">
            
              <form action="<?php echo base_url();?>ChangePassword/submit_edit" method="POST" enctype="multipart/form-data">
                <div>
                  <label>Masukkan Password Baru</label>
                  <input type="password" class="form-control" name="password">
                </div>
                <div>
                  <label>Ketik Ulang Password Baru</label>
                  <input type="password" class="form-control" name="password2">
                </div>
                <span><br></span>
                <div style="float:right">
                  <input type="submit" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('foot');
?>