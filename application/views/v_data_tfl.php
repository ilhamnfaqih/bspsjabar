<?php
  $this->load->view('head');
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Data TFL Anda</h1></div>
            <p><a class="d-none d-sm-inline-block btn btn-sm btn-primary" href="<?php echo base_url();?>Data_tfl/tambah_tfl"><i class="fas fa-plus text-white-50"></i> Tambah TFL</a></p>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid">
      <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>No.</th>
                      <th>NIK</th>
                      <th>Nama</th>
                      <th>Tahap</th>
                      <th>JumlahDampinganDeskel</th>
                      <th>Kot/Kab</th>
                      <th>Kecamatan1</th>
                      <th>Deskel1</th>
                      <th>JumlahDampingan1</th>
                      <th>Kecamatan2</th>
                      <th>Deskel2</th>
                      <th>JumlahDampingan2</th>
                      <th>Kecamatan3</th>
                      <th>Deskel3</th>
                      <th>JumlahDampingan3</th>
                      <th>No.SPK</th>
                      <th>TglSpkKontrak</th>
                      <th>DurasiKontrak</th>
                      <th>BulanAktif</th>
                      <th>JenisKelamin</th>
                      <th>NPWP</th>
                      <th>TempatLahir</th>
                      <th>TanggalLahir</th>
                      <th>AlamatKTP</th>
                      <th>AlamatDomisili</th>
                      <th>PendidikanTerakhir</th>
                      <th>BidangPendidikan</th>
                      <th>No.WA</th>
                      <th>No.HP</th>
                      <th>Email</th>
                      <th>FotoTerbaru</th>
                      <th>Sertifikat</th>
                      <th>Pengalaman</th>
                      <th>Jabatan</th>
                      <th>LamaPengalaman</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Action</th>
                      <th>No.</th>
                      <th>NIK</th>
                      <th>NamaLengkap</th>
                      <th>Tahap</th>
                      <th>JumlahDampinganDeskel</th>
                      <th>Kot/Kab</th>
                      <th>Kecamatan1</th>
                      <th>Deskel1</th>
                      <th>JumlahDampingan1</th>
                      <th>Kecamatan2</th>
                      <th>Deskel2</th>
                      <th>JumlahDampingan2</th>
                      <th>Kecamatan3</th>
                      <th>Deskel3</th>
                      <th>JumlahDampingan3</th>
                      <th>No.SPK</th>
                      <th>TglSpkKontrak</th>
                      <th>DurasiKontrak</th>
                      <th>BulanAktif</th>
                      <th>JenisKelamin</th>
                      <th>NPWP</th>
                      <th>TempatLahir</th>
                      <th>TanggalLahir</th>
                      <th>AlamatKTP</th>
                      <th>AlamatDomisili</th>
                      <th>PendidikanTerakhir</th>
                      <th>BidangPendidikan</th>
                      <th>No.WA</th>
                      <th>No.HP</th>
                      <th>Email</th>
                      <th>FotoTerbaru</th>
                      <th>Sertifikat</th>
                      <th>Pengalaman</th>
                      <th>Jabatan</th>
                      <th>LamaPengalaman</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      <?php 
                        $i = 1;
                        foreach($rows2 as $row){
                      ?>
                        <tr id="<?php echo $row->NIK_tfl; ?>">
                          <td><a href="Data_tfl/edit/<?php echo $row->NIK_tfl; ?>"><i class="fas fa-edit" style="color:orange;"></i></a> <i class="fas fa-trash remove" style="color:red; cursor:pointer;"></i></td>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $row->NIK_tfl; ?></td>
                          <td><?php echo $row->nama_lengkap; ?></td>
                          <td><?php echo $row->tahap; ?></td>
                          <td><?php echo $row->jmlh_dampingan_deskel; ?></td>
                          <td><?php echo $row->kotkab; ?></td>
                          <td><?php echo $row->kecamatan1; ?></td>
                          <td><?php echo $row->deskel1; ?></td>
                          <td><?php echo $row->jmlh_dampingan1; ?></td>
                          <td><?php echo $row->kecamatan2; ?></td>
                          <td><?php echo $row->deskel2; ?></td>
                          <td><?php echo $row->jmlh_dampingan2; ?></td>
                          <td><?php echo $row->kecamatan3; ?></td>
                          <td><?php echo $row->deskel3; ?></td>
                          <td><?php echo $row->jmlh_dampingan3; ?></td>
                          <td><?php echo $row->no_spk; ?></td>
                          <td><?php echo $row->tgl_spk_kontrak; ?></td>
                          <td><?php echo $row->durasi_kontrak; ?></td>
                          <td><?php echo $row->bulan_aktif; ?></td>
                          <td><?php echo $row->jk; ?></td>
                          <td><?php echo $row->npwp_tfl; ?></td>
                          <td><?php echo $row->kabkot_lahir; ?></td>
                          <td><?php echo $row->ttl; ?></td>
                          <td><?php echo $row->alamat_ktp; ?></td>
                          <td><?php echo $row->alamat_domisili; ?></td>
                          <td><?php echo $row->pend_terakhir; ?></td>
                          <td><?php echo $row->bid_pend; ?></td>
                          <td><?php echo $row->no_wa; ?></td>
                          <td><?php echo $row->no_hp; ?></td>
                          <td><?php echo $row->email; ?></td>
                          <td><img src="<?php echo base_url().'uploads/'.$row->foto_terbaru; ?>" width="80px" alt=" Tidak ada foto"></td>
                          <td><?php echo $row->sertifikat; ?></td>
                          <td><?php echo $row->pengalaman; ?></td>
                          <td><?php echo $row->jabatan; ?></td>
                          <td><?php echo $row->lama_pengalaman; ?></td>
                        </tr>
                      <?php
                        $i++;
                        }
                      ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('foot');
?>

<script type="text/javascript">
    $(".remove").click(function(){
        var id = $(this).parents("tr").attr("id");


        if(confirm('Apakah Anda yakin ingin menghapus data ini ?'))
        {
            $.ajax({
               url: '/Data_tfl/hapus/'+id,
               type: 'DELETE',
               error: function() {
                  alert('Maaf data tidak bisa dihapus');
               },
               success: function(data) {
                    $("#"+id).remove();
                    alert("Data Berhasil dihapus!");  
               }
            });
        }
    });


</script>