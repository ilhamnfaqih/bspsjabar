<?php
  $this->load->view('head');
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Tambah Korfas</h1></div>
            <p><a class="d-none d-sm-inline-block btn btn-sm btn-primary" href="<?php echo base_url();?>Data_Korfas"><i class="fas fa-arrow-left text-white-50"></i> Kembali</a></p>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid col-md-6">
      <div class="card shadow mb-4">
      
            <div class="card-body">
            
              <form action="<?php echo base_url();?>Data_korfas/submit_tambah" method="POST">
              <div>
                  <label>Username</label>
                  <input type="text" class="form-control" name="username">
                </div>
                <div>
                  <label>Password</label>
                  <input type="password" class="form-control" name="password">
                </div>
                <div>
                  <label>NIK</label>
                  <input type="text" class="form-control" name="NIK">
                </div>
                <div>
                  <label>No. SPK</label>
                  <input type="text" class="form-control" name="no_spk">
                </div>
                <div>
                  <label>Tahap</label>
                  <input type="text" class="form-control" name="tahap">
                </div>
                <div>
                  <label>Jabatan</label>
                  <input type="text" class="form-control" name="jabatan">
                </div>
                <div>
                  <label>Jumlah Dampingan</label>
                  <input type="number" class="form-control" name="jmlh_dampingan">
                </div>
                <div>
                  <label>Tanggal Kontrak</label>
                  <input type="date" class="form-control" name="tgl_kontrak">
                </div>
                <div>
                  <label>Bulan Aktif</label>
                  <input type="text" class="form-control" name="bulan_aktif">
                </div>
                <div>
                  <label>Nama Lengkap</label>
                  <input type="text" class="form-control" name="nama_lengkap">
                </div>
                <div>
                  <label>Jenis Kelamin</label>
                  <select class="form-control" name="jk">
                    <option value="">--- Pilih Jenis Kelamin ---</option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                </div>
                <div>
                  <label>NPWP</label>
                  <input type="text" class="form-control" name="NPWP">
                </div>
                <div>
                  <label>Tanggal Lahir</label>
                  <input type="date" class="form-control" name="tgl_lahir">
                </div>
                <div>
                  <label>Kabupaten / Kota Lahir</label>
                  <input type="text" class="form-control" name="kabkot_lahir">
                </div>
                <div>
                  <label>Alamat Domisili</label>
                  <input type="text" class="form-control" name="alamat_domisili">
                </div>
                <div>
                  <label>Alamat KTP</label>
                  <input type="text" class="form-control" name="alamat_ktp">
                </div>
                <div>
                  <label>Pendidikan Terakhir</label>
                  <select class="form-control" name="pend_terakhir">
                    <option value="">--- Pilih Pendidikan Terakhir ---</option>
                    <option value="SD">SD</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA/SMK">SMA/SMK</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                </div>
                <div>
                  <label>Bidang Pendidikan</label>
                  <input type="text" class="form-control" name="bid_pend">
                </div>
                <div>
                  <label>No WA</label>
                  <input type="text" class="form-control" name="no_wa">
                </div>
                <div>
                  <label>No HP</label>
                  <input type="text" class="form-control" name="no_hp">
                </div>
                <div>
                  <label>Email</label>
                  <input type="text" class="form-control" name="email">
                </div>
                <div>
                  <label>Sertifikat</label>
                  <input type="text" class="form-control" name="sertifikat">
                </div>
                <div>
                  <label>Pengalaman</label>
                  <input type="text" class="form-control" name="pengalaman">
                </div>
                <div>
                  <label>Jabatan Pengalaman</label>
                  <input type="text" class="form-control" name="jabatan_pengalaman">
                </div>
                <div>
                  <label>Lama Pengalaman</label>
                  <input type="text" class="form-control" name="lama_pengalaman">
                </div>
                <div>
                  <label>Penempatan</label>
                  <input type="text" class="form-control" name="penempatan">
                </div>
                <span><br></span>
                <div style="float:right">
                  <input type="submit" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('foot');
?>