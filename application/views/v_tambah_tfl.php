<?php
  $this->load->view('head');
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Tambah TFL</h1></div>
            <p><a class="d-none d-sm-inline-block btn btn-sm btn-primary" href="<?php echo base_url();?>Data_tfl"><i class="fas fa-arrow-left text-white-50"></i> Kembali</a></p>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid col-md-6">
      <div class="card shadow mb-4">
      
            <div class="card-body">
            
              <form action="<?php echo base_url();?>Data_tfl/submit_tambah" method="POST" enctype="multipart/form-data">
                <div>
                  <label>Username</label>
                  <input type="text" class="form-control" name="username">
                </div>
                <div>
                  <label>Password</label>
                  <input type="password" class="form-control" name="password">
                </div>
                <div>
                  <label>Tahap</label>
                  <input type="text" class="form-control" name="tahap">
                </div>
                <div>
                  <label>Jumlah Dampingan Des/Kel.</label>
                  <input type="text" class="form-control" name="jmlh_dampingan_deskel">
                </div>
                <div>
                  <label>Kota / Kabupaten</label>
                  <input type="text" class="form-control" name="kotkab">
                </div>
                <div>
                  <label>Kecamatan 1</label>
                  <input type="text" class="form-control" name="kecamatan1">
                </div>
                <div>
                  <label>Des/Kel. 1</label>
                  <input type="text" class="form-control" name="deskel1">
                </div>
                <div>
                  <label>Jumlah Dampingan 1</label>
                  <input type="text" class="form-control" name="jmlh_dampingan1">
                </div>
                <div>
                  <label>Kecamatan 2</label>
                  <input type="text" class="form-control" name="kecamatan2">
                </div>
                <div>
                  <label>Des/Kel. 2</label>
                  <input type="text" class="form-control" name="deskel2">
                </div>
                <div>
                  <label>Jumlah Dampingan 2</label>
                  <input type="text" class="form-control" name="jmlh_dampingan2">
                </div>
                <div>
                  <label>Kecamatan 3</label>
                  <input type="text" class="form-control" name="kecamatan3">
                </div>
                <div>
                  <label>Des/Kel. 3</label>
                  <input type="text" class="form-control" name="deskel3">
                </div>
                <div>
                  <label>Jumlah Dampingan 3</label>
                  <input type="text" class="form-control" name="jmlh_dampingan3">
                </div>
                <div>
                  <label>No. SPK</label>
                  <input type="text" class="form-control" name="no_spk">
                </div>
                <div>
                  <label>Tgl SPK Kontrak</label>
                  <input type="date" class="form-control" name="tgl_spk_kontrak">
                </div>
                <div>
                  <label>Durasi Kontrak</label>
                  <input type="text" class="form-control" name="durasi_kontrak">
                </div>
                <div>
                  <label>Bulan Aktif</label>
                  <input type="text" class="form-control" name="bulan_aktif">
                </div>
                <div>
                  <label>Nama Lengkap</label>
                  <input type="text" class="form-control" name="nama_lengkap">
                </div>
                <div>
                  <label>Jenis Kelamin</label>
                  <select class="form-control" name="jk">
                    <option value="">--- Pilih Jenis Kelamin ---</option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                </div>
                <div>
                  <label>NIK TFL</label>
                  <input type="text" class="form-control" name="NIK_tfl">
                </div>
                <div>
                  <label>NPWP TFL</label>
                  <input type="text" class="form-control" name="npwp_tfl">
                </div>
                <div>
                  <label>Kabupaten / Kota Lahir</label>
                  <input type="text" class="form-control" name="kabkot_lahir">
                </div>
                <div>
                  <label>Tanggal Lahir</label>
                  <input type="date" class="form-control" name="ttl">
                </div>
                <div>
                  <label>Alamat KTP</label>
                  <input type="text" class="form-control" name="alamat_ktp">
                </div>
                <div>
                  <label>Alamat Domisili</label>
                  <input type="text" class="form-control" name="alamat_domisili">
                </div>
                <div>
                  <label>Pendidikan Terakhir</label>
                  <select class="form-control" name="pend_terakhir">
                    <option value="">--- Pilih Pendidikan Terakhir ---</option>
                    <option value="SD">SD</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA/SMK">SMA/SMK</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                </div>
                <div>
                  <label>Bidang Pendidikan</label>
                  <input type="text" class="form-control" name="bid_pend">
                </div>
                <div>
                  <label>No WA</label>
                  <input type="text" class="form-control" name="no_wa">
                </div>
                <div>
                  <label>No HP</label>
                  <input type="text" class="form-control" name="no_hp">
                </div>
                <div>
                  <label>Email</label>
                  <input type="text" class="form-control" name="email">
                </div>
                <div>
                  <label>Foto Terbaru <i>*) Max Size Foto 2 Mb</i></label> 
                  <input type="file" class="form-control" name="foto_terbaru">
                </div>
                <div>
                  <label>Sertifikat</label>
                  <input type="text" class="form-control" name="sertifikat">
                </div>
                <div>
                  <label>Pengalaman</label>
                  <input type="text" class="form-control" name="pengalaman">
                </div>
                <div>
                  <label>Jabatan</label>
                  <input type="text" class="form-control" name="jabatan">
                </div>
                <div>
                  <label>Lama Pengalaman</label>
                  <input type="text" class="form-control" name="lama_pengalaman">
                </div>
                <div>
                  <label>Korfas</label>
                  <select type="text" class="form-control" name="NIK_korfas">
                      <option value="">--- Pilih Korfas ---</option>
                    <?php foreach($rows2 as $row){ ?>
                      <option value="<?php echo $row->NIK?>"><?php echo $row->nama_lengkap?></option>
                    <?php } ?>
                  </select>
                </div>
                <span><br></span>
                <div style="float:right">
                  <input type="submit" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('foot');
?>