<?php
  $this->load->view('head');
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dropzone.css">
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Penerima Bantuan</h1>
          </div>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid">
      <div class="card shadow mb-4">
            <div class="card-body">
            <form action="<?php echo base_url();?>Laporan/by_tfl" method="POST">
                    <div class="col-md-6">
                      <select class="form-control form-control-sm" name="NIK_tfl">
                        <option value="">--- Pilih TFL ---</option>
                        <?php foreach($tfl as $tfl){ ?>
                          <option value="<?php echo $tfl->NIK_tfl; ?>"><?php echo $tfl->nama_lengkap; ?></option>
                        <?php } ?>
                      </select>
                          <br>
                      <div class="col-md-8">
                        <input type="submit" class="btn btn-primary" value="Lihat Data" >
                      </div>
                    </div>
                  </form>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  
                  <br>
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kecamatan</th>
                      <th>Desa/Kelurahan</th>
                      <th>Nama</th>
                      <th>No. KTP PB</th>
                      <th>Foto Sebelumnya</th>
                      <th>Foto Terbaru</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Kecamatan</th>
                      <th>Desa/Kelurahan</th>
                      <th>Nama</th>
                      <th>No. KTP PB</th>
                      <th>Foto Sebelumnya</th>
                      <th>Foto Terbaru</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                        $no = 1;
                        foreach($rows2 as $row){
                      ?>
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $row->kecamatan1; ?></td>
                      <td><?php echo $row->deskel1; ?></td>
                      <td><?php echo $row->nama_pb; ?></td>
                      <td><?php echo $row->no_KTP_pb; ?></td>
                      <td>
                          <img src="<?php echo base_url();?>uploads/<?php echo $row->foto_lama; ?>" width="100px" alt=" Tidak ada Foto">
                          <br>
                          <small><b> Tanggal Upload : <?php echo $row->tgl_lama; ?></b></small>
                      </td>
                      <td>
                          <img src="<?php echo base_url();?>uploads/<?php echo $row->foto_baru; ?>" width="100px" alt=" Tidak ada Foto">
                          <br>
                          <small><b> Tanggal Upload : <?php echo $row->tgl_baru; ?></b></small>
                      </td>
                    </tr>
                    <?php
                      $no++;
                        }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('foot');
?>