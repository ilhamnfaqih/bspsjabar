<?php
  $this->load->view('head');
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dropzone.css">
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Foto</h1>
          </div>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid">
      <div class="card shadow mb-4">
            <div class="card-body row">
              <?php foreach($rows as $row){ ?>
                <a href="<?php echo base_url(); ?>/uploads/<?php echo $row->nama_foto; ?>"><img src="<?php echo base_url(); ?>/uploads/<?php echo $row->nama_foto; ?>" style="display:flex; flex-wrap:wrap; width:200px;"></a>
                <span> &nbsp; </span>
              <?php
                }
              ?>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('foot');
?>

<script src="<?php echo base_url() ?>assets/js/dropzone.js"></script>
<script type="text/javascript">

$('#myModal').on('hidden.bs.modal', function () {
 location.reload();
})

$(document).ready(function() {

  $("#openModal").click(function() {
    //remove previous value
    $("#ktp").val("");

    //this'll set id to your modal input
    $("#ktp").val($(this).attr("data-id"));
  });
  
});

  Dropzone.autoDiscover = false;

  var foto_upload= new Dropzone(".dropzone",{
  url: "<?php echo base_url('laporan/proses_upload/') ?>",
  maxFilesize: 5,
  method:"post",
  acceptedFiles:"image/*",
  paramName:"userfile",
  dictInvalidFileType:"Type file ini tidak dizinkan",
  addRemoveLinks:true,
  });


  //Event ketika Memulai mengupload
  foto_upload.on("sending",function(a,b,c){
    a.token=Math.random();
    c.append("token_foto",a.token); //Menmpersiapkan token untuk masing masing foto
    c.append("ktp",<?php echo $row->no_KTP_pb; ?>); 
  });

  
</script>