<?php
  $this->load->view('head');
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dropzone.css">
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Laporan Pengendalian</h1>
          </div>
                <a href="<?php echo base_url(); ?>PhpSpreadsheetController"><button class="d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fa fa-upload"></i> Import Excel</button></a>
                <p>Format Excel yang diupload harus sesuai dengan sistem yang tersedia. untuk mendownload Format Excel yang telah disediakan, silahkan download <a href="<?php echo base_url();?>assets/format.xlsx">di sini.</a></p>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid">
      <div class="card shadow mb-4">
            <div class="card-body">
            <form action="<?php echo base_url();?>Laporan/by_tfl" method="POST">
                    <div class="col-md-6">
                      <select class="form-control form-control-sm" name="NIK_tfl">
                        <option value="">--- Pilih TFL ---</option>
                        <?php foreach($tfl as $tfl){ ?>
                          <option value="<?php echo $tfl->NIK_tfl; ?>"><?php echo $tfl->nama_lengkap; ?></option>
                        <?php } ?>
                      </select>
                          <br>
                      <div class="col-md-8">
                        <input type="submit" class="btn btn-primary" value="Lihat Data" >
                      </div>
                    </div>
                  </form>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  
                  <br>
                  <div class="mb-2 row">
                    <div class="col-md-2">
                      <label>Korfas</label>
                      <input type ="text" class="form-control form-control-sm" value ="<?php echo $tfl->NIK_korfas; ?>" readonly>
                    </div>
                    <div class="col-md-2">
                      <label>Kabupaten/Kota</label>
                      <input type ="text" class="form-control form-control-sm" value ="<?php echo $tfl->kotkab; ?>" readonly>
                    </div>
                    <!--div class="col-md-2">
                      <label>Bulan</label>
                      <input type ="text" value ="Bulan" class="form-control form-control-sm" readonly>
                    </div>
                    <div class="col-md-2">
                      <label>Minggu-Ke</label>
                      <input type ="text" value ="Minggu Ke-" class="form-control form-control-sm" readonly>
                    </div-->
                  </div>
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama TFL</th>
                      <th>Kecamatan</th>
                      <th>Desa/Kelurahan</th>
                      <th>No. BNBA</th>
                      <th>Nama</th>
                      <th>No. KTP PB</th>
                      <th>Alamat</th>
                      <th>Jenis Kelamin</th>
                      <th>Pekerjaan</th>
                      <th>Penghasilan</th>
                      <th>Progress</th>
                      <th>Foto</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Nama TFL</th>
                      <th>Kecamatan</th>
                      <th>Desa/Kelurahan</th>
                      <th>No. BNBA</th>
                      <th>Nama</th>
                      <th>No. KTP PB</th>
                      <th>Alamat</th>
                      <th>Jenis Kelamin</th>
                      <th>Pekerjaan</th>
                      <th>Penghasilan</th>
                      <th>Progress</th>
                      <th>Foto</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                        $no = 1;
                        foreach($rows2 as $row){
                      ?>
                    <tr>
                      <td><a href="<?php echo base_url(); ?>PhpSpreadsheetController/export/<?php echo $row->no_KTP_pb; ?>" class="btn btn-primary btn-sm"><i class="fa fa-download"></i> <?php echo $no; ?></a></td>
                      <td><?php echo $row->nama_lengkap; ?></td>
                      <td><?php echo $row->kecamatan1; ?></td>
                      <td><?php echo $row->deskel1; ?></td>
                      <td><?php echo $row->no_bnba; ?></td>
                      <td><?php echo $row->nama_pb; ?></td>
                      <td><?php echo $row->no_KTP_pb; ?></td>
                      <td><?php echo $row->alamat; ?></td>
                      <td><?php echo $row->jk_pb; ?></td>
                      <td><?php echo $row->pekerjaan_pb; ?></td>
                      <td><?php echo $row->penghasilan_pb; ?></td>
                      <td><?php echo $row->tahap; ?></td>
                      <td width="100%">
                      <button class="d-none d-sm-inline-block btn btn-sm btn-primary openModal" data-id="<?php echo $row->no_KTP_pb; ?>"  id="openModal" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button>
                      <a href="<?php echo base_url(); ?>laporan/lihat_foto/<?php echo $row->no_KTP_pb; ?>">Lihat</a>
                      </td>
                    </tr>
                    <?php
                      $no++;
                        }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->

      <!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Foto</h4>
              <button type="button" class="close" id="closeModal" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <p>Tarik dan Simpan Foto untuk menambahkan. <br><b>(Pastikan hanya file .jpg)</b></p>
              <div class="image_upload_div">
                  
                  <div class="dropzone">
                  <input type="hidden" name="ktp" id="ktp" value="<?php echo $row->no_KTP_pb; ?>">
                  </div>
              </div>
              <p>Rename file yang disarankan : </p>
              <ul>
                  <li>foto_perspektif.jpg</li>
                  <li>foto_tampak_depan.jpg</li>
                  <li>foto_tampak_samping_kanan.jpg</li>
                  <li>foto_tampak_samping_kiri.jpg</li>
                  <li>foto_tampak_belakang.jpg</li>
                  <li>fondasi.jpg</li>
                  <li>sambungan.jpg</li>
                  <li>sloof.jpg</li>
                  <li>overstek.jpg</li>
                  <li>balok.jpg</li>
                  <li>ikatan_balok_dengan_rangka.jpg</li>
                  <li>ikatan_angin.jpg</li>
                  <li>rangka_atap.jpg</li>
                  <li>lantai.jpg</li>
                  <li>dinding.jpg</li>
                  <li>pintu.jpg</li>
                  <li>jendela.jpg</li>
                  <li>sofi_sofi.jpg</li>
                  <li>genteng.jpg</li>
                  <li>kamar_tidur.jpg</li>
                  <li>kamar_mandi.jpg</li>
                  <li>dapur.jpg</li>
              </ul>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
          </div>

        </div>
      </div>
<?php
  $this->load->view('foot');
?>

<script src="<?php echo base_url() ?>assets/js/dropzone.js"></script>
<script type="text/javascript">

$('#myModal').on('hidden.bs.modal', function () {
  location.reload();
})

$(document).ready(function() {

  $(".openModal").click(function() {
    //remove previous value
    $("#ktp").val("");

    //this'll set id to your modal input
    $("#ktp").val($(this).attr("data-id"));
  });
  
});

  Dropzone.autoDiscover = false;

  var foto_upload= new Dropzone(".dropzone",{
    url: "<?php echo base_url('laporan/proses_upload/') ?>",
    maxFilesize: 5,
    resizeWidth: 100,
    quality: 0.6,
    method:"post",
    acceptedFiles:"image/*",
    paramName:"userfile",
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
  });


  //Event ketika Memulai mengupload
  foto_upload.on("sending",function(a,b,c){
    a.token=Math.random();
    c.append("token_foto",a.token); //Menmpersiapkan token untuk masing masing foto
    c.ktp = $('#ktp').val();
    c.append("ktp",c.ktp); 
  });

  
</script>