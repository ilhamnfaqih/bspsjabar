<?php
  $this->load->view('head');
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dropzone.css">
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">My Drive</h1>
          </div>
        </div>
        <!-- /.container-fluid -->
        Tambah File
      <form action="Data_drive_tfl/upload_drive" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="NIK_tfl" value="<?php echo $rows3->NIK_tfl; ?>" class="btn btn-warning">
        <input type="file" name="ktp" class="btn btn-warning">
        <input type="submit" class="btn btn-primary">
      </form>
      <div class="container-fluid">
      <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <br>
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>File</th>
                      <th>Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>File</th>
                      <th>Date</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  <?php 
                        $no = 1;
                        foreach($rows2 as $row){
                      ?>
                    <tr id="<?php echo $row->id; ?>">
                      <td><?php echo $no; ?></td>
                      <td><a href="<?php echo base_url();?>uploads/<?php echo $row->file; ?>"><?php echo $row->file; ?></td>
                      <td><?php echo $row->date; ?></td>
                      <td><button class="btn btn-danger remove"><i class="fa fa-trash"></i></td>
                    </tr>
                    <?php
                      $no++;
                        }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->

<?php
  $this->load->view('foot');
?>

<script type="text/javascript">
    $(".remove").click(function(){
        var id = $(this).parents("tr").attr("id");


        if(confirm('Apakah Anda yakin ingin menghapus data ini ?'))
        {
            $.ajax({
               url: '/Data_drive_tfl/hapus/'+id,
               type: 'DELETE',
               error: function() {
                  alert('Maaf data tidak bisa dihapus');
               },
               success: function(data) {
                    $("#"+id).remove();
                    alert("Data Berhasil dihapus!");  
               }
            });
        }
    });


</script>