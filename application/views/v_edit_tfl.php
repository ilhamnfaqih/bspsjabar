<?php
  $this->load->view('head');
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Edit TFL</h1></div>
            <p><a class="d-none d-sm-inline-block btn btn-sm btn-primary" href="<?php echo base_url();?>Data_tfl"><i class="fas fa-arrow-left text-white-50"></i> Kembali</a></p>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid col-md-6">
      <div class="card shadow mb-4">
      
            <div class="card-body">
            
              <form action="<?php echo base_url();?>Data_tfl/submit_edit" method="POST" enctype="multipart/form-data">
                <div>
                <div>
                  <label>Tahap</label>
                  <input type="text" value="<?php echo $rows2->tahap; ?>" class="form-control" name="tahap">
                </div>
                <div>
                  <label>Jumlah Dampingan Des/Kel.</label>
                  <input type="text" value="<?php echo $rows2->jmlh_dampingan_deskel; ?>" class="form-control" name="jmlh_dampingan_deskel">
                </div>
                <div>
                  <label>Kota / Kabupaten</label>
                  <input type="text"value="<?php echo $rows2->kotkab; ?>" class="form-control" name="kotkab">
                </div>
                <div>
                  <label>Kecamatan 1</label>
                  <input type="text" value="<?php echo $rows2->kecamatan1; ?>" class="form-control" name="kecamatan1">
                </div>
                <div>
                  <label>Des/Kel. 1</label>
                  <input type="text" value="<?php echo $rows2->deskel1; ?>" class="form-control" name="deskel1">
                </div>
                <div>
                  <label>Jumlah Dampingan 1</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->jmlh_dampingan1; ?>" name="jmlh_dampingan1">
                </div>
                <div>
                  <label>Kecamatan 2</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->kecamatan2; ?>" name="kecamatan2">
                </div>
                <div>
                  <label>Des/Kel. 2</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->deskel2; ?>" name="deskel2">
                </div>
                <div>
                  <label>Jumlah Dampingan 2</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->jmlh_dampingan2; ?>" name="jmlh_dampingan2">
                </div>
                <div>
                  <label>Kecamatan 3</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->kecamatan3; ?>" name="kecamatan3">
                </div>
                <div>
                  <label>Des/Kel. 3</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->deskel3; ?>" name="deskel3">
                </div>
                <div>
                  <label>Jumlah Dampingan 3</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->jmlh_dampingan3; ?>" name="jmlh_dampingan3">
                </div>
                <div>
                  <label>No. SPK</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->no_spk; ?>" name="no_spk">
                </div>
                <div>
                  <label>Tgl SPK Kontrak</label>
                  <input type="date" class="form-control" value="<?php echo $rows2->tgl_spk_kontrak; ?>" name="tgl_spk_kontrak">
                </div>
                <div>
                  <label>Durasi Kontrak</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->durasi_kontrak; ?>" name="durasi_kontrak">
                </div>
                <div>
                  <label>Bulan Aktif</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->bulan_aktif; ?>" name="bulan_aktif">
                </div>
                <div>
                  <label>Nama Lengkap</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->nama_lengkap; ?>" name="nama_lengkap">
                </div>
                <div>
                  <label>Jenis Kelamin</label>
                  <select class="form-control" name="jk">
                    <option value="<?php echo $rows2->jk; ?>"><?php echo $rows2->jk; ?></option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                </div>
                <div>
                  <label>NIK TFL</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->NIK_tfl; ?>" name="NIK_tfl" readonly>
                </div>
                <div>
                  <label>NPWP TFL</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->npwp_tfl; ?>" name="npwp_tfl">
                </div>
                <div>
                  <label>Kabupaten / Kota Lahir</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->kabkot_lahir; ?>" name="kabkot_lahir">
                </div>
                <div>
                  <label>Tanggal Lahir</label>
                  <input type="date" class="form-control" value="<?php echo $rows2->ttl; ?>" name="ttl">
                </div>
                <div>
                  <label>Alamat KTP</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->alamat_ktp; ?>" name="alamat_ktp">
                </div>
                <div>
                  <label>Alamat Domisili</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->alamat_domisili; ?>" name="alamat_domisili">
                </div>
                <div>
                  <label>Pendidikan Terakhir</label>
                  <select class="form-control" name="pend_terakhir">
                    <option value="<?php echo $rows2->pend_terakhir; ?>"><?php echo $rows2->pend_terakhir; ?></option>
                    <option value="SD">SD</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA/SMK">SMA/SMK</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                </div>
                <div>
                  <label>Bidang Pendidikan</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->bid_pend; ?>" name="bid_pend">
                </div>
                <div>
                  <label>No WA</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->no_wa; ?>" name="no_wa">
                </div>
                <div>
                  <label>No HP</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->no_hp; ?>" name="no_hp">
                </div>
                <div>
                  <label>Email</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->email; ?>" name="email">
                </div>
                <div>
                  <label>Foto Terbaru <i>*) Max Size Foto 2 Mb</i></label> 
                  <input type="file" class="form-control" name="foto_terbaru">
                  <img src="<?php echo base_url();?>uploads/<?php echo $rows2->foto_terbaru; ?>" width="100px">
                </div>
                <div>
                  <label>Sertifikat</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->sertifikat; ?>" name="sertifikat">
                </div>
                <div>
                  <label>Pengalaman</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->pengalaman; ?>" name="pengalaman">
                </div>
                <div>
                  <label>Jabatan</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->jabatan; ?>" name="jabatan">
                </div>
                <div>
                  <label>Lama Pengalaman</label>
                  <input type="text" class="form-control" value="<?php echo $rows2->lama_pengalaman; ?>" name="lama_pengalaman">
                </div>
                <div>
                  <label>Korfas</label>
                  <select type="text" class="form-control" name="NIK_korfas">
                      <option value="<?php echo $rows3->NIK_korfas; ?>"><?php echo $rows3->NIK_korfas; ?></option>
                    <?php foreach($rows3 as $row){ ?>
                      <option value="<?php echo $row->NIK?>"><?php echo $row->nama_lengkap?></option>
                    <?php } ?>
                  </select>
                </div>
                <span><br></span>
                <div style="float:right">
                  <input type="submit" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('foot');
?>