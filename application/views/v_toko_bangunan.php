<?php
  $this->load->view('head');
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Data Toko Bangunan</h1></div>
            <p><a class="d-none d-sm-inline-block btn btn-sm btn-primary" href="<?php echo base_url();?>Toko_bangunan/tambah_toko_bangunan"><i class="fas fa-plus text-white-50"></i> Tambah Toko Bangunan</a></p>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid">
      <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>NIK</th>
                      <th>Nama Pemilik</th>
                      <th>Nama Toko</th>
                      <th>Kot/Kab</th>
                      <th>No. HP</th>
                      <th>Alamat Toko</th>
                      <th>Foto Toko</th>
                      <th>Bank</th>
                      <th>No. Rek</th>
                      <th>No. SIUP</th>
                      <th>Foto SIUP</th>
                      <th>No. SITUTDP</th>
                      <th>Foto SITUTDP</th>
                      <th>No. NPWP</th>
                      <th>Foto NPWP</th>
                      <th>Layanan Unit 1</th>
                      <th>Layanan DK 1</th>
                      <th>Layanan Unit 2</th>
                      <th>Layanan DK 2</th>
                      <th>Layanan Unit 3</th>
                      <th>Layanan DK 3</th>
                      <th>Layanan Unit 4</th>
                      <th>Layanan DK 4</th>
                      <th>Layanan Unit 5</th>
                      <th>Layanan DK 5</th>
                      <th>TFL</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No.</th>
                      <th>NIK</th>
                      <th>Nama Pemilik</th>
                      <th>Nama Toko</th>
                      <th>Kot/Kab</th>
                      <th>No. HP</th>
                      <th>Alamat Toko</th>
                      <th>Foto Toko</th>
                      <th>Bank</th>
                      <th>No. Rek</th>
                      <th>No. SIUP</th>
                      <th>Foto SIUP</th>
                      <th>No. SITUTDP</th>
                      <th>Foto SITUTDP</th>
                      <th>No. NPWP</th>
                      <th>Foto NPWP</th>
                      <th>Layanan Unit 1</th>
                      <th>Layanan DK 1</th>
                      <th>Layanan Unit 2</th>
                      <th>Layanan DK 2</th>
                      <th>Layanan Unit 3</th>
                      <th>Layanan DK 3</th>
                      <th>Layanan Unit 4</th>
                      <th>Layanan DK 4</th>
                      <th>Layanan Unit 5</th>
                      <th>Layanan DK 5</th>
                      <th>TFL</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      <?php 
                        $i = 1;
                        foreach($rows as $row){
                      ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $row->NIK; ?></td>
                          <td><?php echo $row->nama_pemilik; ?></td>
                          <td><?php echo $row->nama_toko; ?></td>
                          <td><?php echo $row->kabupaten_kota; ?></td>
                          <td><?php echo $row->no_hp; ?></td>
                          <td><?php echo $row->alamat_toko; ?></td>
                          <td><?php echo $row->foto_toko; ?></td>
                          <td><?php echo $row->bank; ?></td>
                          <td><?php echo $row->no_rek; ?></td>
                          <td><?php echo $row->no_siup; ?></td>
                          <td><?php echo $row->foto_siup; ?></td>
                          <td><?php echo $row->no_situtdp; ?></td>
                          <td><?php echo $row->foto_situtdp; ?></td>
                          <td><?php echo $row->no_npwp; ?></td>
                          <td><?php echo $row->foto_npwp; ?></td>
                          <td><?php echo $row->layanan_unit1; ?></td>
                          <td><?php echo $row->layanan_dk1; ?></td>
                          <td><?php echo $row->layanan_unit2; ?></td>
                          <td><?php echo $row->layanan_dk2; ?></td>
                          <td><?php echo $row->layanan_unit3; ?></td>
                          <td><?php echo $row->layanan_dk3; ?></td>
                          <td><?php echo $row->layanan_unit4; ?></td>
                          <td><?php echo $row->layanan_dk4; ?></td>
                          <td><?php echo $row->layanan_unit5; ?></td>
                          <td><?php echo $row->layanan_dk5; ?></td>
                          <td><a href="">Detail / Tambah TFL</a></td>
                        </tr>
                      <?php
                      $i++;
                        }
                      ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('foot');
?>