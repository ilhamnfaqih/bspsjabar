<?php
  $this->load->view('head');
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Tambah Toko Bangunan</h1></div>
            <p><a class="d-none d-sm-inline-block btn btn-sm btn-primary" href="<?php echo base_url();?>Data_Korfas"><i class="fas fa-arrow-left text-white-50"></i> Kembali</a></p>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid col-md-6">
      <div class="card shadow mb-4">
      
            <div class="card-body">
            
              <form action="<?php echo base_url();?>Toko_bangunan/submit_tambah" method="POST" enctype="multipart/form-data">
                <div>
                  <label>NIK</label>
                  <input type="text" class="form-control" name="NIK">
                </div>
                <div>
                  <label>Nama Pemilik</label>
                  <input type="text" class="form-control" name="nama_pemilik">
                </div>
                <div>
                  <label>Nama Toko</label>
                  <input type="text" class="form-control" name="nama_toko">
                </div>
                <div>
                  <label>Kabupaten / Kota</label>
                  <input type="text" class="form-control" name="kabupaten_kota">
                </div>
                <div>
                  <label>No. HP</label>
                  <input type="text" class="form-control" name="no_hp">
                </div>
                <div>
                  <label>Alamat Toko</label>
                  <input type="text" class="form-control" name="alamat_toko">
                </div>
                <div>
                  <label>Foto Toko</label>
                  <input type="file" class="form-control" name="foto_toko">
                </div>
                <div>
                  <label>BANK</label>
                  <input type="text" class="form-control" name="bank">
                </div>
                <div>
                  <label>Nomor Rekening</label>
                  <input type="text" class="form-control" name="no_rek">
                </div>
                <div>
                  <label>No. SIUP</label>
                  <input type="text" class="form-control" name="no_siup">
                </div>
                <div>
                  <label>Foto SIUP</label>
                  <input type="file" class="form-control" name="foto_siup">
                </div>
                <div>
                  <label>No. SITUTDP</label>
                  <input type="text" class="form-control" name="no_situtdp">
                </div>
                <div>
                  <label>Foto SITUTDP</label>
                  <input type="file" class="form-control" name="foto_situtdp">
                </div>
                <div>
                  <label>No. NPWP</label>
                  <input type="text" class="form-control" name="no_npwp">
                </div>
                <div>
                  <label>Foto NPWP</label>
                  <input type="file" class="form-control" name="foto_npwp">
                </div>
                <div>
                  <label>Layanan Unit 1</label>
                  <input type="text" class="form-control" name="layanan_unit1">
                </div>
                <div>
                <div>
                  <label>Layanan DK 1</label>
                  <input type="text" class="form-control" name="layanan_dk1">
                </div>
                <div>
                  <label>Layanan Unit 2</label>
                  <input type="text" class="form-control" name="layanan_unit2">
                </div>
                <div>
                <div>
                  <label>Layanan DK 2</label>
                  <input type="text" class="form-control" name="layanan_dk2">
                </div>
                <div>
                  <label>Layanan Unit 3</label>
                  <input type="text" class="form-control" name="layanan_unit3">
                </div>
                <div>
                <div>
                  <label>Layanan DK 3</label>
                  <input type="text" class="form-control" name="layanan_dk3">
                </div>
                <div>
                  <label>Layanan Unit 4</label>
                  <input type="text" class="form-control" name="layanan_unit4">
                </div>
                <div>
                <div>
                  <label>Layanan DK 4</label>
                  <input type="text" class="form-control" name="layanan_dk4">
                </div>
                <div>
                  <label>Layanan Unit 5</label>
                  <input type="text" class="form-control" name="layanan_unit5">
                </div>
                <div>
                <div>
                  <label>Layanan DK 5</label>
                  <input type="text" class="form-control" name="layanan_dk5">
                </div>
                
                <span><br></span>
                <div style="float:right">
                  <input type="submit" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('foot');
?>