<?php
  $this->load->view('head');
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Data Korfas</h1></div>
            <p><a class="d-none d-sm-inline-block btn btn-sm btn-primary" href="<?php echo base_url();?>Data_korfas/tambah_korfas"><i class="fas fa-plus text-white-50"></i> Tambah Korfas</a></p>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid">
      <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <!--th>Action</th-->
                      <th>No.</th>
                      <th>Tahap</th>
                      <th>Jabatan</th>
                      <th>Jumlah Dampingan</th>
                      <th>No. SPK</th>
                      <th>Tgl Kontrak</th>
                      <th>NIK</th>
                      <th>Bulan Aktif</th>
                      <th>Nama Lengkap</th>
                      <th>Jenis Kelamin</th>
                      <th>NPWP</th>
                      <th>Tgl Lahir</th>
                      <th>Kab/Kota Lahir</th>
                      <th>Alamat Domisili</th>
                      <th>Alamat KTP</th>
                      <th>Pend. Terakhir</th>
                      <th>Bidang Pend.</th>
                      <th>No. WA</th>
                      <th>No. HP</th>
                      <th>Email</th>
                      <th>Sertifikat</th>
                      <th>Pengalaman</th>
                      <th>Jabatan Pengalaman</th>
                      <th>Lama Pengalaman</th>
                      <th>Penempatan</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <!--th>Action</th-->
                      <th>No.</th>
                      <th>Tahap</th>
                      <th>Jabatan</th>
                      <th>Jumlah Dampingan</th>
                      <th>No. SPK</th>
                      <th>Tgl Kontrak</th>
                      <th>NIK</th>
                      <th>Bulan Aktif</th>
                      <th>Nama Lengkap</th>
                      <th>Jenis Kelamin</th>
                      <th>NPWP</th>
                      <th>Tgl Lahir</th>
                      <th>Kab/Kota Lahir</th>
                      <th>Alamat Domisili</th>
                      <th>Alamat KTP</th>
                      <th>Pend. Terakhir</th>
                      <th>Bidang Pend.</th>
                      <th>No. WA</th>
                      <th>No. HP</th>
                      <th>Email</th>
                      <th>Sertifikat</th>
                      <th>Pengalaman</th>
                      <th>Jabatan Pengalaman</th>
                      <th>Lama Pengalaman</th>
                      <th>Penempatan</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      <?php 
                        $i = 1;
                        foreach($rows2 as $row){
                      ?>
                        <tr>
                          <!--td><button class="btn btn-warning"><i class="fas fa-edit"> </i></button><button class="btn btn-danger"><i class="fas fa-trash"></i></button></td-->
                          <td><?php echo $i; ?></td>
                          <td><?php echo $row->tahap; ?></td>
                          <td><?php echo $row->jabatan; ?></td>
                          <td><?php echo $row->jmlh_dampingan; ?></td>
                          <td><?php echo $row->no_spk; ?></td>
                          <td><?php echo $row->tgl_kontrak; ?></td>
                          <td><?php echo $row->NIK; ?></td>
                          <td><?php echo $row->bulan_aktif; ?></td>
                          <td><?php echo $row->nama_lengkap; ?></td>
                          <td><?php echo $row->jk; ?></td>
                          <td><?php echo $row->NPWP; ?></td>
                          <td><?php echo $row->tgl_lahir; ?></td>
                          <td><?php echo $row->kabkot_lahir; ?></td>
                          <td><?php echo $row->alamat_domisili; ?></td>
                          <td><?php echo $row->alamat_ktp; ?></td>
                          <td><?php echo $row->pend_terakhir; ?></td>
                          <td><?php echo $row->bid_pend; ?></td>
                          <td><?php echo $row->no_wa; ?></td>
                          <td><?php echo $row->no_hp; ?></td>
                          <td><?php echo $row->email; ?></td>
                          <td><?php echo $row->sertifikat; ?></td>
                          <td><?php echo $row->pengalaman; ?></td>
                          <td><?php echo $row->jabatan_pengalaman; ?></td>
                          <td><?php echo $row->lama_pengalaman; ?></td>
                          <td><?php echo $row->penempatan; ?></td>
                        </tr>
                      <?php
                        $i++;
                        }
                      ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('foot');
?>