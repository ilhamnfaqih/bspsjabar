<?php
  $this->load->view('head');
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dropzone.css">
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Drive TFL</h1>
          </div>
        </div>
        <!-- /.container-fluid -->

      <div class="container-fluid">
      <div class="card shadow mb-4">
            <div class="card-body">
            <form action="show_drive" method="POST">
              <select class="form-control form-control-sm" name="NIK_tfl">
                  <option value="">--- Pilih TFL ---</option>
                <?php foreach($rows2 as $tfl){ ?>
                  <option value="<?php echo $tfl->NIK_tfl; ?>"><?php echo $tfl->nama_tfl; ?></option>
                <?php } ?>
              </select>
              <br>
              <input type="submit" class="btn btn-primary" value="Lihat Drive">
            </form>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->

<?php
  $this->load->view('foot');
?>