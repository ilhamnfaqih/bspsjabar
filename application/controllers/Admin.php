<?php 
 
class Admin extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('m_login');
		$this->load->model('m_laporan');
		$this->load->model('m_pb');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("welcome"));
		}
	}
 
	function index(){
		if($this->session->userdata('role')=='admin' || $this->session->userdata('role')=='korfas'){
			$data['rows'] = $this->m_login->menu_kabkot();
			$this->load->view('v_admin',$data);
		}else{
			echo "Access Denied";
		}
	}

	function pb($kota){
			$data['rows2'] = $this->m_pb->data($kota);
			$data['tfl'] = $this->m_pb->option_tfl($kota);
			$data['rows'] = $this->m_login->menu_kabkot();
			$this->load->view('v_pb',$data);
	}

	function pb_tfl($kota){
			$data['rows2'] = $this->m_pb->data_on_tfl($kota);
			$data['rows'] = $this->m_login->menu_kabkot();
			$this->load->view('v_pb_tfl',$data);
	}
}