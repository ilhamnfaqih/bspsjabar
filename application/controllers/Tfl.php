<?php 
 
class Tfl extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('m_login');
		$this->load->model('m_pb');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("welcome"));
		}
	}
 
	function index(){
		if($this->session->userdata('role') == 'tfl'){
			$data['rows'] = $this->m_login->menu_kabkot();
			$this->load->view('v_tfl',$data);
		}else{
			echo "Access Denied";
		}
	}

	function proses_upload(){
		$this->db->query('UPDATE penerima_bantuan SET foto_lama = foto_baru');
		$this->db->query('UPDATE penerima_bantuan SET tgl_lama = tgl_baru');

		$ktp=$this->input->post('ktp');
		$tgl_baru = date('dmY');

        $config['upload_path']   = FCPATH.'/uploads/';
		$config['allowed_types'] = 'gif|jpg|png|ico';
        $this->load->library('upload',$config);

        if($this->upload->do_upload('userfile')){
			$nama=$this->upload->data('file_name');			
			$data = array(
				'foto_baru' => $nama,
				'tgl_baru' => $tgl_baru
			);
			$where = array(
				'no_KTP_pb' => $ktp
			);
		
			$this->m_pb->update_data_foto($where,$data,'penerima_bantuan');
		}
	}
}