<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 use PhpOffice\PhpSpreadsheet\Spreadsheet;
 use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class PhpSpreadsheetController extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('m_laporan');
    }

    public function index()
    {
        $this->load->view('spreadsheet');
    }

    public function export($ktp){
        $datas = $this->m_laporan->export_page($ktp);
        $foto_pers = $this->m_laporan->foto_pers($ktp);
        $foto_depan = $this->m_laporan->foto_depan($ktp);
        $foto_kanan = $this->m_laporan->foto_kanan($ktp);
        $foto_kiri = $this->m_laporan->foto_kiri($ktp);
        $foto_belakang = $this->m_laporan->foto_belakang($ktp);
        $fondasi = $this->m_laporan->fondasi($ktp);
        $sambungan = $this->m_laporan->sambungan($ktp);
        $sloof = $this->m_laporan->sloof($ktp);
        $overstek = $this->m_laporan->overstek($ktp);
        $balok = $this->m_laporan->balok($ktp);
        $ikatanbalokdenganrangka = $this->m_laporan->ikatanbalokdenganrangka($ktp);
        $ikatanangin = $this->m_laporan->ikatanangin($ktp);
        $rangkaatap = $this->m_laporan->rangkaatap($ktp);
        $lantai = $this->m_laporan->lantai($ktp);
        $dinding = $this->m_laporan->dinding($ktp);
        $pintu = $this->m_laporan->pintu($ktp);
        $jendela = $this->m_laporan->jendela($ktp);
        $sofisofi = $this->m_laporan->sofisofi($ktp);
        $genteng = $this->m_laporan->genteng($ktp);
        $kamartidur = $this->m_laporan->kamartidur($ktp);
        $kamarmandi = $this->m_laporan->kamarmandi($ktp);
        $dapur = $this->m_laporan->dapur($ktp);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $no = 1;
        foreach($datas as $data){
            $sheet->setCellValue('A1', 'TFL');
            $sheet->setCellValue('B1', $data->nama_lengkap);
            $sheet->setCellValue('A3', 'No.');
            $sheet->setCellValue('A4', $no);
            $sheet->setCellValue('B3', 'Kecamatan');
            $sheet->setCellValue('B4', $data->kecamatan1);
            $sheet->setCellValue('C3', 'Desa/Kelurahan');
            $sheet->setCellValue('C4', $data->deskel1);
            $sheet->setCellValue('D3', 'No. BNBA');
            $sheet->setCellValue('D4', $data->no_bnba);
            $sheet->setCellValue('E3', 'Nama Lengkap');
            $sheet->setCellValue('E4', $data->nama_pb);
            $sheet->setCellValue('F3', 'NIK');
            $sheet->setCellValue('F4', $data->no_KTP_pb);
            $sheet->setCellValue('G3', 'Alamat');
            $sheet->setCellValue('G4', $data->alamat);
            $sheet->setCellValue('H3', 'Jenis Kelamin');
            $sheet->setCellValue('H4', $data->jk_pb);
            $sheet->setCellValue('I3', 'Pekerjaan');
            $sheet->setCellValue('I4', $data->pekerjaan_pb);
            $sheet->setCellValue('I3', 'Penghasilan');
            $sheet->setCellValue('I4', $data->penghasilan_pb);
            $sheet->setCellValue('J3', 'Progress');
            $sheet->setCellValue('J4', $data->tahap);
            $sheet->setCellValue('K3', 'Foto Perspektif');
            $sheet->setCellValue('L3', 'Foto Tampak Depan');
            $sheet->setCellValue('M3', 'Foto Tampak Samping Kanan');
            $sheet->setCellValue('N3', 'Foto Tampak Samping Kiri');
            $sheet->setCellValue('O3', 'Foto Tampak Belakang');
            $sheet->setCellValue('P3', 'Fondasi');
            $sheet->setCellValue('Q3', 'Sambungan');
            $sheet->setCellValue('R3', 'Sloof');
            $sheet->setCellValue('S3', 'Overstek');
            $sheet->setCellValue('T3', 'Balok');
            $sheet->setCellValue('U3', 'Ikatan Balok Dengan Rangka');
            $sheet->setCellValue('V3', 'Ikatan Angin');
            $sheet->setCellValue('W3', 'Rangka Atap');
            $sheet->setCellValue('X3', 'Lantai');
            $sheet->setCellValue('Y3', 'Dinding');
            $sheet->setCellValue('Z3', 'Pintu');
            $sheet->setCellValue('AA3', 'Jendela');
            $sheet->setCellValue('AB3', 'Sofi-sofi');
            $sheet->setCellValue('AC3', 'Genteng');
            $sheet->setCellValue('AD3', 'Kamar Tidur');
            $sheet->setCellValue('AE3', 'Kamar Mandi');
            $sheet->setCellValue('AF3', 'Dapur');
            $no++;
        }
        
        foreach($foto_pers as $foto_pers){
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('gambar');
            $drawing->setDescription('gambar');
            $drawing->setPath('uploads/'.$foto_pers->nama_foto); // put your path and image here
            $drawing->setCoordinates('K4');
            $drawing->setHeight(100);
            $drawing->setwidth(100);
            $drawing->setResizeProportional(true);
            $drawing->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($foto_depan as $foto_depan){
            $drawing2 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing2->setName('gambar');
            $drawing2->setDescription('gambar');
            $drawing2->setPath('uploads/'.$foto_depan->nama_foto); // put your path and image here
            $drawing2->setCoordinates('L4');
            $drawing2->setHeight(100);
            $drawing2->setwidth(100);
            $drawing2->setResizeProportional(true);
            $drawing2->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($foto_kanan as $foto_kanan){
            $drawing3 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing3->setName('gambar');
            $drawing3->setDescription('gambar');
            $drawing3->setPath('uploads/'.$foto_kanan->nama_foto); // put your path and image here
            $drawing3->setCoordinates('M4');
            $drawing3->setHeight(100);
            $drawing3->setwidth(100);
            $drawing3->setResizeProportional(true);
            $drawing3->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($foto_kiri as $foto_kiri){
            $drawing4 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing4->setName('gambar');
            $drawing4->setDescription('gambar');
            $drawing4->setPath('uploads/'.$foto_kiri->nama_foto); // put your path and image here
            $drawing4->setCoordinates('N4');
            $drawing4->setHeight(100);
            $drawing4->setwidth(100);
            $drawing4->setResizeProportional(true);
            $drawing4->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($foto_belakang as $foto_belakang){
            $drawing5 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing5->setName('gambar');
            $drawing5->setDescription('gambar');
            $drawing5->setPath('uploads/'.$foto_belakang->nama_foto); // put your path and image here
            $drawing5->setCoordinates('O4');
            $drawing5->setHeight(100);
            $drawing5->setwidth(100);
            $drawing5->setResizeProportional(true);
            $drawing5->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($fondasi as $fondasi){
            $drawing6 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing6->setName('gambar');
            $drawing6->setDescription('gambar');
            $drawing6->setPath('uploads/'.$fondasi->nama_foto); // put your path and image here
            $drawing6->setCoordinates('P4');
            $drawing6->setHeight(100);
            $drawing6->setwidth(100);
            $drawing6->setResizeProportional(true);
            $drawing6->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($sambungan as $sambungan){
            $drawing7 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing7->setName('gambar');
            $drawing7->setDescription('gambar');
            $drawing7->setPath('uploads/'.$sambungan->nama_foto); // put your path and image here
            $drawing7->setCoordinates('Q4');
            $drawing7->setHeight(100);
            $drawing7->setwidth(100);
            $drawing7->setResizeProportional(true);
            $drawing7->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($sloof as $sloof){
            $drawing8 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing8->setName('gambar');
            $drawing8->setDescription('gambar');
            $drawing8->setPath('uploads/'.$sloof->nama_foto); // put your path and image here
            $drawing8->setCoordinates('R4');
            $drawing8->setHeight(100);
            $drawing8->setwidth(100);
            $drawing8->setResizeProportional(true);
            $drawing8->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($overstek as $overstek){
            $drawing9 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing9->setName('gambar');
            $drawing9->setDescription('gambar');
            $drawing9->setPath('uploads/'.$overstek->nama_foto); // put your path and image here
            $drawing9->setCoordinates('S4');
            $drawing9->setHeight(100);
            $drawing9->setwidth(100);
            $drawing9->setResizeProportional(true);
            $drawing9->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($balok as $balok){
            $drawing10 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing10->setName('gambar');
            $drawing10->setDescription('gambar');
            $drawing10->setPath('uploads/'.$balok->nama_foto); // put your path and image here
            $drawing10->setCoordinates('T4');
            $drawing10->setHeight(100);
            $drawing10->setwidth(100);
            $drawing10->setResizeProportional(true);
            $drawing10->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($ikatanbalokdenganrangka as $ikatanbalokdenganrangka){
            $drawing11 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing11->setName('gambar');
            $drawing11->setDescription('gambar');
            $drawing11->setPath('uploads/'.$ikatanbalokdenganrangka->nama_foto); // put your path and image here
            $drawing11->setCoordinates('U4');
            $drawing11->setHeight(100);
            $drawing11->setwidth(100);
            $drawing11->setResizeProportional(true);
            $drawing11->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($ikatanangin as $ikatanangin){
            $drawing12 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing12->setName('gambar');
            $drawing12->setDescription('gambar');
            $drawing12->setPath('uploads/'.$ikatanangin->nama_foto); // put your path and image here
            $drawing12->setCoordinates('V4');
            $drawing12->setHeight(100);
            $drawing12->setwidth(100);
            $drawing12->setResizeProportional(true);
            $drawing12->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($rangkaatap as $rangkaatap){
            $drawing13 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing13->setName('gambar');
            $drawing13->setDescription('gambar');
            $drawing13->setPath('uploads/'.$rangkaatap->nama_foto); // put your path and image here
            $drawing13->setCoordinates('W4');
            $drawing13->setHeight(100);
            $drawing13->setwidth(100);
            $drawing13->setResizeProportional(true);
            $drawing13->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($lantai as $lantai){
            $drawing14 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing14->setName('gambar');
            $drawing14->setDescription('gambar');
            $drawing14->setPath('uploads/'.$lantai->nama_foto); // put your path and image here
            $drawing14->setCoordinates('X4');
            $drawing14->setHeight(100);
            $drawing14->setwidth(100);
            $drawing14->setResizeProportional(true);
            $drawing14->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($dinding as $dinding){
            $drawing15 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing15->setName('gambar');
            $drawing15->setDescription('gambar');
            $drawing15->setPath('uploads/'.$dinding->nama_foto); // put your path and image here
            $drawing15->setCoordinates('Y4');
            $drawing15->setHeight(100);
            $drawing15->setwidth(100);
            $drawing15->setResizeProportional(true);
            $drawing15->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($pintu as $pintu){
            $drawing16 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing16->setName('gambar');
            $drawing16->setDescription('gambar');
            $drawing16->setPath('uploads/'.$pintu->nama_foto); // put your path and image here
            $drawing16->setCoordinates('Z4');
            $drawing16->setHeight(100);
            $drawing16->setwidth(100);
            $drawing16->setResizeProportional(true);
            $drawing16->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($jendela as $jendela){
            $drawing17 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing17->setName('gambar');
            $drawing17->setDescription('gambar');
            $drawing17->setPath('uploads/'.$jendela->nama_foto); // put your path and image here
            $drawing17->setCoordinates('AA4');
            $drawing17->setHeight(100);
            $drawing17->setwidth(100);
            $drawing17->setResizeProportional(true);
            $drawing17->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($sofisofi as $sofisofi){
            $drawing18 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing18->setName('gambar');
            $drawing18->setDescription('gambar');
            $drawing18->setPath('uploads/'.$sofisofi->nama_foto); // put your path and image here
            $drawing18->setCoordinates('AB4');
            $drawing18->setHeight(100);
            $drawing18->setwidth(100);
            $drawing18->setResizeProportional(true);
            $drawing18->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($genteng as $genteng){
            $drawing19 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing19->setName('gambar');
            $drawing19->setDescription('gambar');
            $drawing19->setPath('uploads/'.$genteng->nama_foto); // put your path and image here
            $drawing19->setCoordinates('AC4');
            $drawing19->setHeight(100);
            $drawing19->setwidth(100);
            $drawing19->setResizeProportional(true);
            $drawing19->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($kamartidur as $kamartidur){
            $drawing20 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing20->setName('gambar');
            $drawing20->setDescription('gambar');
            $drawing20->setPath('uploads/'.$kamartidur->nama_foto); // put your path and image here
            $drawing20->setCoordinates('AD4');
            $drawing20->setHeight(100);
            $drawing20->setwidth(100);
            $drawing20->setResizeProportional(true);
            $drawing20->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($kamarmandi as $kamarmandi){
            $drawing21 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing21->setName('gambar');
            $drawing21->setDescription('gambar');
            $drawing21->setPath('uploads/'.$kamarmandi->nama_foto); // put your path and image here
            $drawing21->setCoordinates('AE4');
            $drawing21->setHeight(100);
            $drawing21->setwidth(100);
            $drawing21->setResizeProportional(true);
            $drawing21->setWorksheet($spreadsheet->getActiveSheet());
        }

        foreach($dapur as $dapur){
            $drawing22 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing22->setName('gambar');
            $drawing22->setDescription('gambar');
            $drawing22->setPath('uploads/'.$dapur->nama_foto); // put your path and image here
            $drawing22->setCoordinates('AF4');
            $drawing22->setHeight(100);
            $drawing22->setwidth(100);
            $drawing22->setResizeProportional(true);
            $drawing22->setWorksheet($spreadsheet->getActiveSheet());
        }

        $writer = new Xlsx($spreadsheet);
        $filename = date('dmYhis');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file
    }
    public function import(){
        $file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if(isset($_FILES['upload_file']['name']) && in_array($_FILES['upload_file']['type'], $file_mimes)) {
                $arr_file = explode('.', $_FILES['upload_file']['name']);
                $extension = end($arr_file);
                if('csv' == $extension){
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }
                    $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
                    $sheetData = $spreadsheet->getActiveSheet()->toArray();
                //echo "<pre>";
                //print_r($sheetData);

                $i = 0;
                foreach($sheetData as $key=>$val)
                {
                    $data[$i]['nama_pb'] = $val[0];
                    $data[$i]['no_bnba'] = $val[1];
                    $data[$i]['no_KTP_pb'] = $val[2];
                    $data[$i]['ttl_pb'] = $val[3];
                    $data[$i]['jk_pb'] = $val[4];
                    $data[$i]['pekerjaan_pb'] = $val[5];
                    $data[$i]['telp_pb'] = $val[6];
                    $data[$i]['penghasilan_pb'] = $val[7];
                    $data[$i]['penghuni_pb'] = $val[8];
                    $data[$i]['alamat'] = $val[9];
                    $data[$i]['no_kpb'] = $val[10];
                    $data[$i]['bentuk_bantuan'] = $val[11];
                    $data[$i]['jenis_kegiatan'] = $val[12];
                    $data[$i]['luas_rumah'] = $val[13];
                    $data[$i]['kondisi_lantai'] = $val[14];
                    $data[$i]['kondisi_dinding'] = $val[15];
                    $data[$i]['kondisi_atap'] = $val[16];
                    $data[$i]['jenis_kerusakan'] = $val[17];
                    $data[$i]['kebutuhan_khusus'] = $val[18];
                    $data[$i]['kodepos'] = $val[19];
                    $data[$i]['kepala_desa'] = $val[20];
                    $data[$i]['bukti_tanah'] = $val[21];
                    $data[$i]['kelengkapan_util'] = $val[22];
                    $data[$i]['jumlah_bantuan'] = $val[23];
                    $data[$i]['nilai_proposal'] = $val[24];
                    $data[$i]['nilai_swadaya'] = $val[25];
                    $data[$i]['tng_kerja'] = $val[26];
                    $data[$i]['kondisi_rmhstrk'] = $val[27];
                    $data[$i]['kondisi_rmhnon'] = $val[28];
                    $data[$i]['mck'] = $val[29];
                    $data[$i]['NIK_tfl'] = $val[30];
                    $data[$i]['no_sk'] = $val[31];
                    $data[$i]['toko_penyalur'] = $val[32];
                    $data[$i]['tahap'] = $val[33];
                    $data[$i]['sumber_dana'] = $val[34];
                    $i++;
                }
                $this->db->insert_on_duplicate_update_batch('penerima_bantuan', $data);
                //$this->db->insert_batch('penerima_bantuan', $data);
                redirect('Laporan');
            }
        }
}