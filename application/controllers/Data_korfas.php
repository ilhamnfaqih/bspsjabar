<?php 
 
class Data_Korfas extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('m_login');
		$this->load->model('m_korfas');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("welcome"));
		}
	}
 
	function index(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$data['rows'] = $this->m_login->menu_kabkot();
			$data['rows2'] = $this->m_korfas->data();
			$this->load->view('v_data_korfas',$data);
		}else{
			echo "Access Denied";
		}
	}

	function tambah_korfas(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$this->load->view('v_tambah_korfas');
		}else{
			echo "Access Denied";
		}
	}

	function submit_tambah(){
		$this->load->helper('string');
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$get_token = random_string('alnum', 16);
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$tahap = $this->input->post('tahap');
			$jabatan = $this->input->post('jabatan');
			$jmlh_dampingan = $this->input->post('jmlh_dampingan');
			$no_spk = $this->input->post('no_spk');
			$tgl_kontrak = $this->input->post('tgl_kontrak');
			$NIK = $this->input->post('NIK');
			$bulan_aktif = $this->input->post('bulan_aktif');
			$nama_lengkap = $this->input->post('nama_lengkap');
			$jk = $this->input->post('jk');
			$NPWP = $this->input->post('NPWP');
			$tgl_lahir = $this->input->post('tgl_lahir');
			$kabkot_lahir = $this->input->post('kabkot_lahir');
			$alamat_domisili = $this->input->post('alamat_domisili');
			$alamat_ktp = $this->input->post('alamat_ktp');
			$pend_terakhir = $this->input->post('pend_terakhir');
			$bid_pend = $this->input->post('bid_pend');
			$no_wa = $this->input->post('no_wa');
			$no_hp = $this->input->post('no_hp');
			$email = $this->input->post('email');
			$sertifikat = $this->input->post('sertifikat');
			$pengalaman = $this->input->post('pengalaman');
			$jabatan_pengalaman = $this->input->post('jabatan_pengalaman');
			$lama_pengalaman = $this->input->post('lama_pengalaman');
			$penempatan = $this->input->post('penempatan');
	
			$data2 = array(
				'username' => $username,
				'password' => md5($password),
				'token_user' => $get_token,
				'role' => 'korfas',
				'nama' => $nama_lengkap
			);
			$data = array(
				'tahap' => $tahap,
				'jabatan' => $jabatan,
				'jmlh_dampingan' => $jmlh_dampingan,
				'no_spk' => $no_spk,
				'tgl_kontrak' => $tgl_kontrak,
				'NIK' => $NIK,
				'bulan_aktif' => $bulan_aktif,
				'nama_lengkap' => $nama_lengkap,
				'jk' => $jk,
				'NPWP' => $NPWP,
				'tgl_lahir' => $tgl_lahir,
				'kabkot_lahir' => $kabkot_lahir,
				'alamat_domisili' => $alamat_domisili,
				'alamat_ktp' => $alamat_ktp,
				'pend_terakhir' => $pend_terakhir,
				'bid_pend' => $bid_pend,
				'no_wa' => $no_wa,
				'no_hp' => $no_hp,
				'email' => $email,
				'sertifikat' => $sertifikat,
				'pengalaman' => $pengalaman,
				'jabatan_pengalaman' => $jabatan_pengalaman,
				'lama_pengalaman' => $lama_pengalaman,
				'penempatan' => $penempatan,
				'token_user' => $get_token
				);
			$this->m_korfas->input_data($data,'tb_korfas');
			$this->m_korfas->input_data($data2,'user');
			redirect('Data_korfas');
		}else{
			echo "Access Denied";
		}
	}
}