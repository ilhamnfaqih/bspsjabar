<?php 
class Toko_bangunan extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('m_login');
		$this->load->model('m_toko_bangunan');
		$this->load->helper(array('url','file'));
		if($this->session->userdata('status') != "login"){
			redirect(base_url("welcome"));
		}
	}
 
	function index(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$data['rows'] = $this->m_toko_bangunan->data();
			$this->load->view('v_toko_bangunan',$data);
		}else{
			echo "Access Denied";
		}
	}

	function tambah_toko_bangunan(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$this->load->view('v_tambah_toko_bangunan');
		}else{
			echo "Access Denied";
		}
	}
	
	function submit_tambah(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
				
			$config['upload_path']   = FCPATH.'/uploads/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$this->load->library('upload',$config);

			if($this->upload->do_upload('foto')){
					$NIK = $this->input->post('NIK');
					$nama_pemilik = $this->input->post('nama_pemilik');
					$nama_toko = $this->input->post('nama_toko');
					$kabupaten_kota = $this->input->post('kabupaten_kota');
					$no_hp = $this->input->post('no_hp');
					$alamat_toko = $this->input->post('alamat_toko');
					$foto_toko = $this->upload->data('file_name');
					$bank = $this->input->post('bank');
					$no_rek = $this->input->post('no_rek');
					$no_siup = $this->input->post('no_siup');
					$foto_siup =  $this->upload->data('file_name');
					$no_situtdp = $this->input->post('no_situtdp');
					$foto_situtdp =  $this->upload->data('file_name');
					$no_npwp = $this->input->post('no_npwp');
					$foto_npwp =  $this->upload->data('file_name');
					$layanan_unit1 = $this->input->post('layanan_unit1');
					$layanan_dk1 = $this->input->post('layanan_dk1');
					$layanan_unit2 = $this->input->post('layanan_unit2');
					$layanan_dk2 = $this->input->post('layanan_dk2');
					$layanan_unit3 = $this->input->post('layanan_unit3');
					$layanan_dk3 = $this->input->post('layanan_dk3');
					$layanan_unit4 = $this->input->post('layanan_unit4');
					$layanan_dk4 = $this->input->post('layanan_dk4');
					$layanan_unit5 = $this->input->post('layanan_unit5');
					$layanan_dk5 = $this->input->post('layanan_dk5');

				$data = array(
					'NIK' => $NIK,
					'nama_pemilik' => $nama_pemilik,
					'nama_toko' => $nama_toko,
					'kabupaten_kota' => $kabupaten_kota,
					'no_hp' => $no_hp,
					'alamat_toko' => $alamat_toko,
					'foto_toko' => $foto_toko,
					'bank' => $bank,
					'no_rek' => $no_rek,
					'no_siup' => $no_siup,
					'foto_siup' => $foto_siup,
					'no_situtdp' => $no_situtdp,
					'foto_situtdp' => $foto_situtdp,
					'no_npwp' => $no_npwp,
					'foto_npwp' => $foto_npwp,
					'layanan_unit1' => $layanan_unit1,
					'layanan_dk1' => $layanan_dk1,
					'layanan_unit2' => $layanan_unit2,
					'layanan_dk2' => $layanan_dk2,
					'layanan_unit3' => $layanan_unit3,
					'layanan_dk3' => $layanan_dk3,
					'layanan_unit4' => $layanan_unit4,
					'layanan_dk4' => $layanan_dk4,
					'layanan_unit5' => $layanan_unit5,
					'layanan_dk5' => $layanan_dk5,
					);
				
				$this->m_toko_bangunan->input_data($data,'toko_bangunan');
				redirect('Toko_bangunan');
			}else{
				$this->upload->display_errors('<p>', '</p>');
			}
		}else{
			echo "Access Denied";
		}
	}
}