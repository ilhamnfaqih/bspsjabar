<?php 
class Laporan extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('m_login');
		$this->load->model('m_laporan');
		$this->load->helper(array('url','file'));
		if($this->session->userdata('status') != "login"){
			redirect(base_url("welcome"));
		}
	}
 
	function index(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$data['rows2'] = $this->m_laporan->data();
			$data['tfl'] = $this->m_laporan->option_tfl();
			$data['rows'] = $this->m_login->menu_kabkot();
			$this->load->view('v_laporan',$data);
		}else{
			echo "Access Denied";
		}
	}

	function by_tfl(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$NIK_tfl = $this->input->post('NIK_tfl');
			$data['rows'] = $this->m_laporan->by_tfl($NIK_tfl);
			$data['tfl'] = $this->m_laporan->option_tfl();
			$this->load->view('v_laporan',$data);
		}else{
			echo "Access Denied";
		}
	}

	function export($ktp){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$data['rows'] = $this->m_laporan->export_page($ktp);
			$data['rows2'] = $this->m_laporan->foto($ktp);
			$this->load->view('v_export',$data);
		}else{
			echo "Access Denied";
		}
	}

	function lihat_foto($ktp){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$data['rows'] = $this->m_laporan->foto($ktp);
			$this->load->view('v_laporan_foto',$data);
		}else{
			echo "Access Denied";
		}
	}

	function proses_upload(){
		$ktp=$this->input->post('ktp');
		$token=$this->input->post('token_foto');

        $config['upload_path']   = FCPATH.'/uploads/';
		$config['allowed_types'] = 'gif|jpg|png|ico';
        $this->load->library('upload',$config);

		
        if($this->upload->do_upload('userfile')){
        	$nama=$this->upload->data('file_name');
        	$this->db->insert('foto_pb',array('no_KTP_pb'=>$ktp,'nama_foto'=>$nama,'token_foto'=>$token));
		}
	}
}