<?php 
 
class Data_drive_tfl extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('m_login');
		$this->load->model('m_tfl');
		$this->load->model('m_korfas');
		$this->load->helper(array('url','file'));
		if($this->session->userdata('status') != "login"){
			redirect(base_url("welcome"));
		}
	}
 
	function index(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'tfl'){
			$data['rows3'] = $this->m_tfl->data_drive_single();
			$data['rows2'] = $this->m_tfl->data_drive();
			$data['rows'] = $this->m_login->menu_kabkot();
			$this->load->view('v_my_drive',$data);
		}else{
			echo "Access Denied";
		}
	}

	function upload_drive(){
		$config['upload_path']   = FCPATH.'/uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|docx|xls|csv|xlsx|ppt|pptx';
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			$this->upload->do_upload('ktp');
				$NIK_tfl = $this->input->post('NIK_tfl');
				$nama=$this->upload->data('file_name');

				$data = array(
					'NIK_tfl' => $NIK_tfl,
					'file' => $nama,
					);
				
				$this->m_tfl->input_drive($data,'drive_tfl');
				redirect('Data_drive_tfl');
	}
	
	function hapus($id){
		$this->db->delete('drive_tfl', array('id' => $id));
	}
}