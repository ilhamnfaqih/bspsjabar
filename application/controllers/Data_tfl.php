<?php 
 
class Data_tfl extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('m_login');
		$this->load->model('m_tfl');
		$this->load->model('m_korfas');
		$this->load->helper(array('url','file'));
		if($this->session->userdata('status') != "login"){
			redirect(base_url("welcome"));
		}
	}
 
	function index(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$data['rows2'] = $this->m_tfl->data();
			$data['rows'] = $this->m_login->menu_kabkot();
			$this->load->view('v_data_tfl',$data);
		}else{
			echo "Access Denied";
		}
	}

	function tambah_tfl(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$data['rows2'] = $this->m_korfas->data();
			$data['rows'] = $this->m_login->menu_kabkot();
			$this->load->view('v_tambah_tfl',$data);
		}else{
			echo "Access Denied";
		}
	}

	function edit($NIK_tfl){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$data['rows3'] = $this->m_korfas->data();
			$data['rows'] = $this->m_login->menu_kabkot();
			$data['rows2'] = $this->m_tfl->edit($NIK_tfl);
			$this->load->view('v_edit_tfl',$data);
		}else{
			echo "Access Denied";
		}
	}

	function submit_tambah(){
		$this->load->helper('string');
		

		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$get_token = random_string('alnum', 16);
			$config['upload_path']   = FCPATH.'/uploads/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$this->load->library('upload',$config);

			$this->upload->do_upload('foto_terbaru');

				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$tahap = $this->input->post('tahap');
				$jmlh_dampingan_deskel = $this->input->post('jmlh_dampingan_deskel');
				$kotkab = $this->input->post('kotkab');
				$kecamatan1 = $this->input->post('kecamatan1');
				$deskel1 = $this->input->post('deskel1');
				$jmlh_dampingan1 = $this->input->post('jmlh_dampingan1');
				$kecamatan2 = $this->input->post('kecamatan2');
				$deskel2 = $this->input->post('deskel2');
				$jmlh_dampingan2 = $this->input->post('jmlh_dampingan2');
				$kecamatan3 = $this->input->post('kecamatan3');
				$deskel3 = $this->input->post('deskel3');
				$jmlh_dampingan3 = $this->input->post('jmlh_dampingan3');
				$no_spk = $this->input->post('no_spk');
				$tgl_spk_kontrak = $this->input->post('tgl_spk_kontrak');
				$durasi_kontrak = $this->input->post('durasi_kontrak');
				$bulan_aktif = $this->input->post('bulan_aktif');
				$nama_lengkap = $this->input->post('nama_lengkap');
				$jk = $this->input->post('jk');
				$NIK_tfl = $this->input->post('NIK_tfl');
				$npwp_tfl = $this->input->post('npwp_tfl');
				$kabkot_lahir = $this->input->post('kabkot_lahir');
				$ttl = $this->input->post('ttl');
				$alamat_ktp = $this->input->post('alamat_ktp');
				$alamat_domisili = $this->input->post('alamat_domisili');
				$pend_terakhir = $this->input->post('pend_terakhir');
				$bid_pend = $this->input->post('bid_pend');
				$no_wa = $this->input->post('no_wa');
				$no_hp = $this->input->post('no_hp');
				$email = $this->input->post('email');
				$sertifikat = $this->input->post('sertifikat');
				$pengalaman = $this->input->post('pengalaman');
				$jabatan = $this->input->post('jabatan');
				$lama_pengalaman = $this->input->post('lama_pengalaman');
				$NIK_korfas = $this->input->post('NIK_korfas');
				$foto_terbaru=$this->upload->data('file_name');


				$data2 = array(
					'username' => $username,
					'password' => md5($password),
					'token_user' => $get_token,
					'role' => 'tfl',
					'nama' => $nama_lengkap
				);
				$data = array(
					'tahap' => $tahap,
					'jmlh_dampingan_deskel' => $jmlh_dampingan_deskel,
					'kotkab' => $kotkab,
					'kecamatan1' => $kecamatan1,
					'deskel1' => $deskel1,
					'jmlh_dampingan1' => $jmlh_dampingan1,
					'kecamatan2' => $kecamatan2,
					'deskel2' => $deskel2,
					'jmlh_dampingan2' => $jmlh_dampingan2,
					'kecamatan3' => $kecamatan3,
					'deskel3' => $deskel3,
					'jmlh_dampingan3' => $jmlh_dampingan3,
					'no_spk' => $no_spk,
					'tgl_spk_kontrak' => $tgl_spk_kontrak,
					'durasi_kontrak' => $durasi_kontrak,
					'bulan_aktif' => $bulan_aktif,
					'nama_lengkap' => $nama_lengkap,
					'jk' => $jk,
					'NIK_tfl' => $NIK_tfl,
					'npwp_tfl' => $npwp_tfl,
					'kabkot_lahir' => $kabkot_lahir,
					'ttl' => $ttl,
					'alamat_ktp' => $alamat_ktp,
					'alamat_domisili' => $alamat_domisili,
					'pend_terakhir' => $pend_terakhir,
					'bid_pend' => $bid_pend,
					'no_wa' => $no_wa,
					'no_hp' => $no_hp,
					'email' => $email,
					'foto_terbaru' => $foto_terbaru,
					'sertifikat' => $sertifikat,
					'pengalaman' => $pengalaman,
					'jabatan' => $jabatan,
					'lama_pengalaman' => $lama_pengalaman,
					'NIK_korfas' => $NIK_korfas,
					'token_user' => $get_token
					);
				
				$this->m_korfas->input_data($data,'tb_tfl');
				$this->m_korfas->input_data($data2,'user');
				redirect('Data_tfl');
		}else{
			echo "Access Denied";
		}
	}

	function submit_edit(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
				
			$config['upload_path']   = FCPATH.'/uploads/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$this->load->library('upload',$config);

			$this->upload->do_upload('foto_terbaru');

				$tahap = $this->input->post('tahap');
				$jmlh_dampingan_deskel = $this->input->post('jmlh_dampingan_deskel');
				$kotkab = $this->input->post('kotkab');
				$kecamatan1 = $this->input->post('kecamatan1');
				$deskel1 = $this->input->post('deskel1');
				$jmlh_dampingan1 = $this->input->post('jmlh_dampingan1');
				$kecamatan2 = $this->input->post('kecamatan2');
				$deskel2 = $this->input->post('deskel2');
				$jmlh_dampingan2 = $this->input->post('jmlh_dampingan2');
				$kecamatan3 = $this->input->post('kecamatan3');
				$deskel3 = $this->input->post('deskel3');
				$jmlh_dampingan3 = $this->input->post('jmlh_dampingan3');
				$no_spk = $this->input->post('no_spk');
				$tgl_spk_kontrak = $this->input->post('tgl_spk_kontrak');
				$durasi_kontrak = $this->input->post('durasi_kontrak');
				$bulan_aktif = $this->input->post('bulan_aktif');
				$nama_lengkap = $this->input->post('nama_lengkap');
				$jk = $this->input->post('jk');
				$NIK_tfl = $this->input->post('NIK_tfl');
				$npwp_tfl = $this->input->post('npwp_tfl');
				$kabkot_lahir = $this->input->post('kabkot_lahir');
				$ttl = $this->input->post('ttl');
				$alamat_ktp = $this->input->post('alamat_ktp');
				$alamat_domisili = $this->input->post('alamat_domisili');
				$pend_terakhir = $this->input->post('pend_terakhir');
				$bid_pend = $this->input->post('bid_pend');
				$no_wa = $this->input->post('no_wa');
				$no_hp = $this->input->post('no_hp');
				$email = $this->input->post('email');
				$sertifikat = $this->input->post('sertifikat');
				$pengalaman = $this->input->post('pengalaman');
				$jabatan = $this->input->post('jabatan');
				$lama_pengalaman = $this->input->post('lama_pengalaman');
				$NIK_korfas = $this->input->post('NIK_korfas');
				$foto_terbaru=$this->upload->data('file_name');

				$data = array(
					'tahap' => $tahap,
					'jmlh_dampingan_deskel' => $jmlh_dampingan_deskel,
					'kotkab' => $kotkab,
					'kecamatan1' => $kecamatan1,
					'deskel1' => $deskel1,
					'jmlh_dampingan1' => $jmlh_dampingan1,
					'kecamatan2' => $kecamatan2,
					'deskel2' => $deskel2,
					'jmlh_dampingan2' => $jmlh_dampingan2,
					'kecamatan3' => $kecamatan3,
					'deskel3' => $deskel3,
					'jmlh_dampingan3' => $jmlh_dampingan3,
					'no_spk' => $no_spk,
					'tgl_spk_kontrak' => $tgl_spk_kontrak,
					'durasi_kontrak' => $durasi_kontrak,
					'bulan_aktif' => $bulan_aktif,
					'nama_lengkap' => $nama_lengkap,
					'jk' => $jk,
					'NIK_tfl' => $NIK_tfl,
					'npwp_tfl' => $npwp_tfl,
					'kabkot_lahir' => $kabkot_lahir,
					'ttl' => $ttl,
					'alamat_ktp' => $alamat_ktp,
					'alamat_domisili' => $alamat_domisili,
					'pend_terakhir' => $pend_terakhir,
					'bid_pend' => $bid_pend,
					'no_wa' => $no_wa,
					'no_hp' => $no_hp,
					'email' => $email,
					'foto_terbaru' => $foto_terbaru,
					'sertifikat' => $sertifikat,
					'pengalaman' => $pengalaman,
					'jabatan' => $jabatan,
					'lama_pengalaman' => $lama_pengalaman,
					'NIK_korfas' => $NIK_korfas
					);
			
				$where = array(
					'NIK_tfl' => $NIK_tfl
				);
			
				$this->m_tfl->update_data($where,$data,'tb_tfl');
				redirect('Data_tfl');
		}else{
			echo "Access Denied";
		}
	}

	function hapus($id){
		$this->db->delete('tb_tfl', array('NIK_tfl' => $id));
	}
}