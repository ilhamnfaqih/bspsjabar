<?php 
 
class ChangePassword extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('m_login');
		$this->load->model('m_admin');
		$this->load->helper(array('url','file'));
		if($this->session->userdata('status') != "login"){
			redirect(base_url("welcome"));
		}
	}
 
	function index(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas' || $this->session->userdata('role') == 'tfl'){
			$data['rows'] = $this->m_login->menu_kabkot();
			$this->load->view('v_change_password',$data);
		}else{
			echo "Access Denied";
		}
	}


	function submit_edit(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas' || $this->session->userdata('role') == 'tfl'){
				$password = $this->input->post('password');
				$password2 = $this->input->post('password2');

				if($password != $password2){
					?> 
						<script>
							alert('Password tidak Sama');
							history.back();
						</script>
					<?php
				}else{
					$sess_id = $this->session->userdata('id_user');
					$data = array(
						'password' => md5($password),
						);
				
					$where = array(
						'id_user' => $sess_id
					);
				
					$this->m_admin->update_data($where,$data,'user');
					?>
						<script>
							alert('Password berhasil diganti!');
							history.back();
						</script>
					<?php
					}
		}else{
			echo "Access Denied";
		}
	}
}