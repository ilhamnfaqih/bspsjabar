<?php 
 
class Korfas extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('m_login');
		$this->load->model('m_tfl');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("welcome"));
		}
	}
 
	function index(){
		if($this->session->userdata('role') == 'korfas'){
			$data['rows'] = $this->m_login->menu_kabkot();
			$this->load->view('v_korfas',$data);
		}else{
			echo "Access Denied";
		}
	}

	function drive_korfas(){
		if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'korfas'){
			$data['rows2'] = $this->m_tfl->data();
			$data['rows'] = $this->m_login->menu_kabkot();
			$this->load->view('v_drive_korfas',$data);
		}else{
			echo "Access Denied";
		}
	}

	function show_drive(){
		$NIK_tfl = $this->input->post('NIK_tfl');
		$data['rows2'] = $this->m_tfl->show_drive($NIK_tfl);
		$data['rows'] = $this->m_login->menu_kabkot();
		$this->load->view('v_show_drive',$data);
	}
}